import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Subscription, interval } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { userService } from '../_services/user.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-course',
  templateUrl: './course-percentage.component.html',
})
export class CoursePercentageComponent implements OnInit {
  public options: any = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Course by percentage'
    },
    subtitle: {
        text: 'Source: Mphasis'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: []
    }]
}
subscription: Subscription;
constructor(private http: HttpClient, private userService: userService) { }

ngOnInit(){
  
  let reqHeader  = new HttpHeaders({'Authorization': 'Bearer '+ localStorage.getItem('currentUser')});
  
  this.http.get<any[]>('http://localhost:58373/api/course', {headers: reqHeader}).subscribe(data => {

      for(var row of data){
          this.options.series[0]['data'].push({ name:row['sourceName'],y:row['percentage']});
      }
      Highcharts.chart('container', this.options);
    },
    error => {
      console.log('Something went wrong.');
    });
}

}