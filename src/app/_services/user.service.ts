import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class userService{

 constructor(private http: HttpClient){ }

 public register(user){
     const body={
        employeeId: user.employeeId, 
        projectName: user.projectName , 
        email: user.email, 
        mobile: user.mobile, 
        firstName: user.firstName, 
        lastName: user.lastName, 
        username: user.username, 
        password: user.password
     };
     const reqHeader  = new HttpHeaders({'Content-Type': 'application/json'});

     return this.http.post("http://localhost:58373/api/registration", body, {headers: reqHeader});
 }

 public getUsers(){
    const reqHeader  = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.get<any>("http://localhost:58373/api/user",{headers: reqHeader});
 }
}