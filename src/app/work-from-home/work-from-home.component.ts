import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Subscription, interval } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { userService } from '../_services/user.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  templateUrl: './work-from-home.component.html',
})
export class WorkFromHomeComponent implements OnInit {
  public options: any = {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Attendance of WFH employees'
    },
    subtitle: {
        text: 'Source: Mphasis'
    },
    xAxis: {
        categories: [],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'No of Days',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' days'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 5,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'No of days present',
        data: []
    }, {
        name: 'No of days absent',
        data: []
    }, {
        name: 'Total no of days',
        data: []
    }]
}
subscription: Subscription;
constructor(private http: HttpClient, private userService: userService) { }

ngOnInit(){

  let reqHeader  = new HttpHeaders({'Authorization': 'Bearer '+ localStorage.getItem('currentUser')});
  
  this.http.get<any[]>('http://localhost:58373/api/Attendance?worktype=WFH',{headers: reqHeader}).subscribe(data => {

      for(var row of data){
        this.options.xAxis.categories.push(row['employeeName']);
        (row['noOfDaysPresent']==0)?this.options.series[0]['data'].push(null):this.options.series[0]['data'].push(row['noOfDaysPresent']);
        (row['noOfDaysAbsent']==0)?this.options.series[1]['data'].push(null):this.options.series[1]['data'].push(row['noOfDaysAbsent']);
        (row['totalNoOfDays']==0)?this.options.series[2]['data'].push(null):this.options.series[2]['data'].push(row['totalNoOfDays']);
      }
              
      Highcharts.chart('container', this.options);
    },
    error => {
      console.log('Something went wrong.');
    });
}

}