import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';
import { AttendanceComponent } from './attendance-graph/attendance-graph.component';
import { ResourceGraphComponent } from './resource-graph/resource-graph.component';
import { CoursePercentageComponent } from './course-percentage/course-percentage.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { WorkFromHomeComponent } from './work-from-home/work-from-home.component';
import { WorkFromOfficeComponent } from './work-from-office/work-from-office.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard], 
    children:[
        {path:'attendance', component:AttendanceComponent},
        {path:'', component:ResourceGraphComponent},
        {path:'courses', component: CoursePercentageComponent},
        {path:'courseDetails', component: CourseDetailsComponent},
        {path:'wfh', component:WorkFromHomeComponent},
        {path:'wfo', component:WorkFromOfficeComponent}
    ] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);