import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Subscription, interval } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { userService } from '../_services/user.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance-graph.component.html',
})
export class AttendanceComponent implements OnInit {
  public options: any = {

    title: {
        text: 'Attendance details of SPADES WFH/WFO employees, April'
    },

    subtitle: {
        text: 'Source: Mphasis'
    },

    yAxis: {
        title: {
            text: 'Number of present per day'
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: 'Range: 01 to 30'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 1
        }
    },

    series: [
        {
        name: 'WFH',
        data: []
    }, 
    {
        name: 'WFO',
        data: []
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

}
subscription: Subscription;
constructor(private http: HttpClient, private userService: userService) { }

ngOnInit(){

  let reqHeader  = new HttpHeaders({'Authorization': 'Bearer '+ localStorage.getItem('currentUser')});
  
  this.http.get<any[]>('http://localhost:58373/api/Attendance',{headers: reqHeader}).subscribe(data => {

      for(var row of data){
        this.options.series[0]['data'].push(row['wfcPresent']);
        (row['wfoPresent']==0)?this.options.series[1]['data'].push(null):this.options.series[1]['data'].push(row['wfoPresent']);
      }
              
      Highcharts.chart('container', this.options);
    },
    error => {
      console.log('Something went wrong.');
    });
}

}