import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Subscription, interval } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { userService } from '../_services/user.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  templateUrl: './resource-graph.component.html',
})
export class ResourceGraphComponent implements OnInit {
  public options: any = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'SPADES project distribution'
    },
    subtitle: {
        text: 'Source: Mphasis'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: 0,
            style: {
                fontSize: '16px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'No of Resources'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Resource count as of April: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
		colorByPoint: true,
        data: [
        ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '16px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
}
subscription: Subscription;
constructor(private http: HttpClient, private userService: userService) { }

ngOnInit(){
  // Set 10 seconds interval to update data again and again
  const source = interval(10000);

  // Sample API
  let reqHeader  = new HttpHeaders({'Authorization': 'Bearer '+ localStorage.getItem('currentUser')});
  
  this.http.get<any[]>('http://localhost:58373/api/user', {headers: reqHeader}).subscribe(data => {
      let DAOCOUNT = 4;
      let SANKALPCOUNT = 5;
      let SQUADRONCOUNT= 7;
      let THING2COUNT = 9;
      let STRIKERSCOUNT=8;
      let MAYHEMCOUNT =7;
      let UNKNOWNPROJECT=8;

      for(var row of data){
          var project:string = row['projectName'];
            switch(project) {
             case 'DAO':{
              DAOCOUNT++; break;
             }
             case 'SQUADRON':{
               SQUADRONCOUNT++; break;
             }
             case 'SANKALP':{
               SANKALPCOUNT++; break;
             }
             case 'THING2':{
               THING2COUNT++; break;
             }
             case 'STRIKERS': {
               STRIKERSCOUNT++; break;
             }
             case 'MAYHEM':{
               MAYHEMCOUNT++; break;
             }
             default:{
              UNKNOWNPROJECT++;
             }
             
            }
      }
              
      this.options.series[0]['data'] = [['DAO', DAOCOUNT],['SANKALP', SANKALPCOUNT],['SQUADRON', SQUADRONCOUNT],['THING2', THING2COUNT],['STRIKERS', STRIKERSCOUNT],['MAYHEM', MAYHEMCOUNT],['UNKNOWN',UNKNOWNPROJECT++]];
      Highcharts.chart('container', this.options);
    },
    error => {
      console.log('Something went wrong.');
    });
}

}