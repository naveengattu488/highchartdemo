import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Subscription, interval } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { userService } from '../_services/user.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  templateUrl: './course-details.component.html',
})
export class CourseDetailsComponent implements OnInit {
  public options: any = {

    chart: {
        type: 'column',
        width: 1200
    },

    title: {
        text: 'Course details per day'
    },

    subtitle: {
        text: 'Source: Mphasis'
    },

    legend: {
        align: 'right',
        verticalAlign: 'middle',
        layout: 'vertical'
    },

    xAxis: {
        categories: [],
        labels: {
            x: -10
        },
    },

    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Amount'
        }
    },

    series: [{
        name: 'No of courses started',
        data: []
    }, {
        name: 'No of courses completed',
        data: []
    }, {
        name: 'No of hours spent',
        data: []
    }],

    responsive: {
        rules: [{
            chartOptions: {
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    layout: 'horizontal'
                },
                yAxis: {
                    labels: {
                        align: 'left',
                        x: 0,
                        y: -5
                    },
                    title: {
                        text: null
                    }
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                }
            }
        }]
    }
}
subscription: Subscription;
constructor(private http: HttpClient, private userService: userService) { }

ngOnInit(){

  let reqHeader  = new HttpHeaders({'Authorization': 'Bearer '+ localStorage.getItem('currentUser')});
  
  this.http.get<any[]>('http://localhost:58373/api/coursedetails',{headers: reqHeader}).subscribe(data => {

      for(var row of data){
        this.options.xAxis.categories.push(row['day']);
        (row['coursesBegan']==0)?this.options.series[0]['data'].push(null):this.options.series[0]['data'].push(row['coursesBegan']);
        (row['coursesCompleted']==0)?this.options.series[1]['data'].push(null):this.options.series[1]['data'].push(row['coursesCompleted']);
        (row['noOfHoursSpent']==0)?this.options.series[2]['data'].push(null):this.options.series[2]['data'].push(row['noOfHoursSpent']);
      }
              
      Highcharts.chart('container', this.options);
    },
    error => {
      console.log('Something went wrong.');
    });
}

}