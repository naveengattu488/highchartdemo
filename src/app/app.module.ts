import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { appRoutingModule } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AttendanceComponent } from './attendance-graph/attendance-graph.component';
import { ResourceGraphComponent } from './resource-graph/resource-graph.component';
import { CoursePercentageComponent } from './course-percentage/course-percentage.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { WorkFromHomeComponent } from './work-from-home/work-from-home.component';
import { WorkFromOfficeComponent } from './work-from-office/work-from-office.component';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        AttendanceComponent,
        ResourceGraphComponent,
        CoursePercentageComponent,
        CourseDetailsComponent,
        WorkFromHomeComponent,
        WorkFromOfficeComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }

    ],
    bootstrap: [AppComponent]
})
export class AppModule { };